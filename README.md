<img align="right" width="100" src="https://user-images.githubusercontent.com/1472352/49656357-1e874080-fa78-11e8-80ea-69e2103345cf.png" alt="EasyWeChat Logo" expires="2021-08-13" />

<h1 align="left"><a href="https://gitee.com/King_first/extension-for-laravel">ExtensionForLaravel</a></h1>

📦 。



## Requirement

1. PHP >=5.5.9
2. laravel/framework >=5.1.*
3. **[Composer](https://getcomposer.org/)**
4. openssl 拓展  
5. pdo 拓展
6. fileinfo 拓展（素材管理模块需要用到）

## Installation  

1.composer 安装 
```shell
$ composer require xianlin/extension-for-laravel dev-master
```

2.在laravel config/app.php 的providers里添加服务提供者 
```shell
$ Xianlin\ExtensionForLaravel\ExtensionServiceProvider::class
```

3.发布配置
```shell
$  php artisan vendor:publish --provider="Xianlin\ExtensionForLaravel\ExtensionServiceProvider"
```


## Usage

基本使用:

##### 1.创建扩展包  
    ```shell
    $ php artisan extension:create xianlin/phpinfo --namespace=XianLin\PHPInfo
    ```
   其中xianlin/phpinfo是包名，namespace选项是这个包使用的顶级命名空间，运行这个命令之后, 将会在在config/extension.php中设置的扩展目录中生成目录xianlin/phpinfo和下面的文件结构
   ~~~
   ├── LICENSE
       ├── README.md
       ├── composer.json
       ├── database
       │   ├── migrations
       │   └── seeds
       ├── resources
       │   ├── assets
       │   └── views
       │       └── index.blade.php
       ├── routes
       │   └── web.php
       └── src
           ├── PHPInfo.php
           ├── PHPInfoServiceProvider.php
           └── Http
               └── Controllers
                   └── PHPInfoController.php
   ~~~
  
  ~~~
      1.resources用来放置视图文件和静态资源文件,  
      2.src主要用来放置逻辑代码,   
      3.routes/web.php用来存放这个扩展的路由设置，  
      4.database用来放置数据库迁移文件和数据seeders。
  ~~~
  
##### 2.  功能开发
这个扩展的功能主要用来将PHP中的phpinfo函数所显示的页面，集成进系统应用中，它将会有一个路由和一个视图文件，没有数据库文件和静态资源文件，我们可以将没有用到的文件或目录清理掉，清理之后的目录文件为：  
~~~
      ├── LICENSE
        ├── README.md
        ├── composer.json
        ├── resources
        │   └── views
        │       └── index.blade.php
        ├── routes
        │   └── web.php
        └── src
            ├── PHPInfo.php
            ├── PHPInfoServiceProvider.php
            └── Http
                └── Controllers
                    └── PHPInfoController.php
~~~
  
生成完扩展框架之后，你可能需要一边调试一边开发，所以可以参考下面的 <a href="#31--本地扩展安装">3.1 本地扩展安装</a> ，先把扩展安装进系统中，继续开发  


###### 2.1  添加路由
首先添加一个路由，在routes/web.php中已经自动生成好了一个路由配置
```php
<?php

Route::get('phpinfo', PHPInfoController::class.'@index');

```
XianLin\PHPInfo\Http\Controllers\PHPInfoController控制器的index方法来处理这个请求。  



###### 2.2  加载视图  
如果这个扩展需要加载视图文件，在src/PHPInfoServiceProvider.php的handle方法中加入以下的代码：  
```php
if ($views = $extension->views()) {
    $this->loadViewsFrom($views, 'phpinfo');
}
```  

loadViewsFrom方法的的第一个参数为在扩展类src/PHPInfo.php中设置的视图属性，第二个参数是视图文件目录的命名空间，设置为phpinfo之后，在控制器中用view('phpinfo::index')来加载resources/views目录下的视图文件

###### 2.3  引入静态资源  
如果你的项目中有静态资源文件需要引入，先把文件放在resources/assets目录中，比如放入resources/assets/foo.js和resources/assets/bar.css这两个文件  
  
  接着在扩展类src/PHPInfo.php中设置$assets属性  
    
  ~~~
    public $assets = __DIR__.'/../resources/assets';
  ~~~
  
然后在src/PHPInfoServiceProvider.php的handle方法中设置发布目录  
```php
if ($this->app->runningInConsole() && $assets = $extension->assets()) {
    $this->publishes(
        [$assets => public_path('vendor/xianlin/phpinfo')],
        'phpinfo'
    );
}
```  
安装完成之后，运行php artisan vendor:publish --provider=Xianlin\PHPInfo\PHPInfoServiceProvider，文件将会复制到public/vendor/xianlin/phpinfo目录中。  

###### 2.4  代码逻辑开发  
在 Extensions\xianlin\phpinfo 下可以增加所需要的代码业务逻辑  


###### 3 扩展安装  

###### 3.1 本地扩展安装  
在开发的过程中，一般需要一边调试一边开发，所以先按照下面的方式进行本地安装 

```$xslt
"repositories": [
    {
        "type": "path",
        "url": "app/extensions/xianlin/phpinfo"
    }
]
```  

然后运行 以下命令之一 完成安装

```angular2
composer require xianlin/phpinfo
``` 
或者

```angular2
composer require xianlin/phpinfo @dev
```

或者

```angular2
composer require xianlin/phpinfo dev-master
```

如果有静态文件需要发布，运行下面的命令

```$xslt
php artisan vendor:publish --provider=Xianlin\PHPInfo\PHPInfoServiceProvider
```

这样就完成了安装，打开http://localhost/phpinfo  访问这个扩展


  
更多请参考 [https://gitee.com/King_first/extension-for-laravel/](https://gitee.com/King_first/extension-for-laravel/)。

## Documentation


## License

MIT
